-- THis creates th database with intial data

create database phone;

use phone;

create table numbers(
  name varchar(50),
  number varchar(30)
);

insert into numbers values('Steve Shilling', '555-1234');
insert into numbers values('Sheheryar Butt', '555-5555');
insert into numbers values('Aayush Khan', '123-5142'); 
